#ws@2023 igw.tf

##################################################################
# Setup VPC's Internet Gateway
##################################################################

resource "aws_internet_gateway" "default" {
  # Provides a resource to create a VPC Internet Gateway.

  # Note(!!!): It's recommended to denote that the AWS Instance or Elastic IP depends on the Internet Gateway.
  count = (local.enabled && var.vpc_enable_internet_gateway) ? 1 : 0

  # (Required) The VPC ID to create in.
  vpc_id = join("", aws_vpc.default.*.id)

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-igw", local.vpc_name),
    },
  )
}




