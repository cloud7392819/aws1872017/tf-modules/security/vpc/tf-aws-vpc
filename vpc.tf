#ws@2023 vpc.tf

##################################################################
# Setup VPC and some of related resources
##################################################################

resource "aws_vpc" "default" {
  
  # Provides a VPC resource.
  count = local.enabled ? 1 : 0

  # (Required) The CIDR block for the VPC.
  cidr_block = var.vpc_cidr_block

  # (Optional) A tenancy option for instances launched into the VPC
  instance_tenancy = var.vpc_instance_tenancy
  
  # (Optional) A boolean flag to enable/disable DNS support in the VPC.
  enable_dns_support = var.vpc_enable_dns_support
  
  # (Optional) A boolean flag to enable/disable DNS hostnames in the VPC.
  enable_dns_hostnames = var.vpc_enable_dns_hostnames
  
  # (Optional) Requests an Amazon-provided IPv6 CIDR block with a /56 prefix length for the VPC. You cannot specify
  # the range of IP addresses, or the size of the CIDR block.
  assign_generated_ipv6_cidr_block = false

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = local.vpc_name
      #Name = format("%s", local.vpc_name),
    },
  )
}



