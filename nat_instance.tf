# ws@2023 nat_instance.tf

##################################################################
# NAT Gateway [instance]
##################################################################

##################################################################
## Network/Security resources
##################################################################

resource "aws_security_group" "nat_instance" {

  # Provides a security group resource.
  count = (local.enabled && local.subnets_nat_resource_create && local.nat_instances_amount > 0) ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-nat-sg-", var.subnets_vpc_name)

  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  description = "Security Group for NAT Instances."

  # (Optional, Forces new resource) The VPC ID.
  #vpc_id = var.subnets_vpc_id
  vpc_id = join ("", aws_vpc.default.*.id)

  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  tags = merge(
    local.tags,
    {
      Name = format("%s-nat-sg", var.subnets_vpc_name),
    },
  )
}

resource "aws_security_group_rule" "instance_allow_all_egress" {

  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && local.subnets_nat_resource_create && local.nat_instances_amount > 0) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"

  # (Optional) Description of the rule.
  description = "Allow ALL Egress traffic from the NAT instances."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0

  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0

  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"

  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.nat_instance.*.id)
}

resource "aws_security_group_rule" "instance_allow_vpc_ingress" {

  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && local.subnets_nat_resource_create && local.nat_instances_amount > 0) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"

  # (Optional) Description of the rule.
  description = "Allow ALL ingress traffic from the VPC CIDR block."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0

  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0

  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"

  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = [local.vpc_cidr_block]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.nat_instance.*.id)
}

##################################################################
## NAT Instance
##################################################################

# Simulate the following CLI command to get the latest version of Amazon provided EC2 AMI for NAT instances:
#   aws --region us-east-1 \
#       ec2 describe-images \
#           --owners amazon \
#           --filters \
#               Name="name",Values="amzn-ami-vpc-nat*" \
#               Name="virtualization-type",Values="hvm"


resource "aws_eip" "nat_instance" {

  # Provides an Elastic IP resource.

  # Note(1): EIP may require IGW to exist prior to association. Use depends_on to set an explicit dependency on the IGW.

  # Note(2): Do not use `network_interface` to associate the EIP to `aws_lb` or `aws_nat_gateway` resources. Instead use
  # the `allocation_id` available in those resources to allow AWS to manage the association, otherwise you will see
  # `AuthFailure` errors.
  count = (local.enabled && local.subnets_nat_resource_create && var.subnets_nat_instance_static_eip) ? local.nat_instances_amount : 0

  # See:
  #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
  depends_on = [
    local.subnets_module_depends_on
  ]

  # (Optional) Boolean if the EIP is in a VPC or not.
  #vpc = true

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.default.names[count.index],
      Name             = format("%s-%s-nat-instance-eip", var.subnets_vpc_name, data.aws_availability_zones.default.names[count.index]),
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}


data "aws_ami" "nat_instance" {

  # Use this data source to get the ID of a registered AMI for use in other resources.
  count = (local.enabled && local.subnets_nat_resource_create && local.nat_instances_amount > 0) ? 1 : 0

  # (Required) List of AMI owners to limit search. At least 1 value must be specified. Valid values:
  #   * an AWS account ID
  #   * self (the current account)
  #   * an AWS owner alias (e.g. amazon, aws-marketplace, microsoft).
  owners = ["amazon"]

  # (Optional) If more than one result is returned, use the most recent AMI.
  most_recent = true

  # (Optional) One or more name/value pairs to filter off of. There are several valid keys, for a full reference,
  # check out describe-images in the AWS CLI reference:
  #   * http://docs.aws.amazon.com/cli/latest/reference/ec2/describe-images.html
  filter {
    name   = "name"
    values = ["amzn-ami-vpc-nat*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}


# https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-comparison.html
# https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html
# https://dzone.com/articles/nat-instance-vs-nat-gateway

resource "aws_instance" "nat_instance" {

  # Provides an EC2 instance resource. This allows instances to be created, updated, and deleted. Instances also support
  # provisioning.
  count = (local.enabled && local.subnets_nat_resource_create) ? local.nat_instances_amount : 0


  # (Required) The AMI to use for the instance.
  ami = join("", data.aws_ami.nat_instance.*.id)

  # (Optional) The AZ to start the instance in.
  availability_zone = data.aws_availability_zones.default.names[count.index]

  # (Optional) If true, enables EC2 Instance Termination Protection
  disable_api_termination = false

  # (Required) The type of instance to start. Updates to this field will trigger a stop/start of the EC2 instance.
  instance_type = var.subnets_nat_instance_type

  # NOTE: If an instance is being created in a VPC, `vpc_security_group_ids` must be used instead of `security_groups`.
  # (Optional, VPC only) A list of security group IDs to associate with.
  #vpc_security_group_ids = [aws_security_group.nat_instance[0].id]
  vpc_security_group_ids = [aws_security_group.nat_instance[count.index].id]

  # (Optional) The VPC Subnet ID to launch in.
  subnet_id = matchkeys(
    aws_subnet.public.*.id,
    aws_subnet.public.*.tags.AvailabilityZone,
    [data.aws_availability_zones.default.names[count.index]]
  )[0]

  # (Optional) Associate a public ip address with an instance in a VPC. Boolean value.
  associate_public_ip_address = true

  # (Optional) Controls if traffic is routed to the instance when the destination address does not match the instance.
  # Used for NAT or VPNs. See:
  #   * https://docs.aws.amazon.com/vpc/latest/userguide/VPC_NAT_Instance.html#EIP_Disable_SrcDestCheck
  source_dest_check = false

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.default.names[count.index],
      Name             = format("%s-%s-nat-instance", var.subnets_vpc_name, data.aws_availability_zones.default.names[count.index]),
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_eip_association" "nat_instance" {

  # Provides an AWS EIP Association as a top level resource, to associate and disassociate Elastic IPs from AWS Instances
  # and Network Interfaces.

  # NOTE(1):
  # Do not use this resource to associate an EIP to `aws_lb` or `aws_nat_gateway` resources. Instead use
  # the `allocation_id` available in those resources to allow AWS to manage the association, otherwise you will see
  # `AuthFailure` errors.

  # NOTE(2): `aws_eip_association` is useful in scenarios where EIPs are either pre-existing or distributed to customers
  # or users and therefore cannot be changed.
  count = (local.enabled && local.subnets_nat_resource_create && var.subnets_nat_instance_static_eip) ? local.nat_instances_amount : 0

  # (Optional) The allocation ID. This is required for EC2-VPC.
  allocation_id = aws_eip.nat_instance[count.index].id

  # (Optional) The ID of the instance. This is required for EC2-Classic. For EC2-VPC, you can specify either the
  # instance ID or the network interface ID, but not both. The operation fails if you specify an instance ID unless
  # exactly one network interface is attached.
  instance_id = aws_instance.nat_instance[count.index].id
}

# if NAT instance enabled, every route from Private subnets must points to either NAT instance in the same AZ (in case
# of High Availability config) or the single NAT instance created in one of available AZs.

resource "aws_route" "private_to_nat_instance" {

  # Provides a resource to create a routing table entry (a route) in a VPC routing table.

  # NOTE on Route Tables and Routes:
  # Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined in-line.
  # At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources. Doing so
  # will cause a conflict of rule settings and will overwrite rules.
  count = (local.enabled && local.subnets_nat_resource_create && local.nat_instances_amount > 0) ? var.subnets_az_amount : 0

  # (Required) The ID of the routing table.
  #route_table_id = aws_route_table.private[count.index].id
  route_table_id = aws_route_table.private[0].id

  network_interface_id   = element(aws_instance.nat_instance[*].primary_network_interface_id, count.index)

  # (Optional) The destination CIDR block.
  destination_cidr_block = "0.0.0.0/0"

  # (Optional) Identifier of an EC2 instance.
  # Select NAT instance either those located in the same AZ (in case of HA config), or the single one.
  #instance_id = local.nat_instances_amount > 1 ? matchkeys(aws_instance.nat_instance.*.id, aws_instance.nat_instance.*.tags.AvailabilityZone, [aws_route_table.private[count.index].tags.AvailabilityZone])[0] : aws_instance.nat_instance[0].id

  depends_on             = [
    aws_route_table.private
  ]
}



