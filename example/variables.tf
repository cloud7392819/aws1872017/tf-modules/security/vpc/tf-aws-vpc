# ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

# AWS Region
variable "region" {
  description	= "AWS Region"
  default 		= null
}

# Application name
variable "name" {
	description = "Application name"
	default		= null
}

# Application name
variable "cut_name" {
	description = "Application cut name"
	default		= null
}

# --- Environment ---
# Environment name
variable "environment" {
	description = "Environment name"
	default		= "backend"
}

# Owner Email
variable "ownerEmail" {
	description = "Owner Email"
	default		= null
}

# Team
variable "team" {
	description = "Team"
	default		= null
}

# DeployBy
variable "deployedby" {
	description = "DeployedBy"
	default		= null
}

##################################################################
# Domain settings
##################################################################

# domain name default
variable "domain_name_default" {
  description = "Domain zone"
  default     = null
}

# domain name first
variable "domain_name_first" {
  description = "Domain zone"
  default     = null
}

# domain name second
variable "domain_name_second" {
  description = "Domain zone"
  default     = null
}

##################################################################
# EC2 settings
##################################################################

# public key
variable "public_key" {
  description = "Public key"
  default     = null
}
