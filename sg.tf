#ws@2023 sg.tf

##################################################################
# Setup VPC's "Default" Security Group
##################################################################

resource "aws_default_security_group" "default" {
    
  # Provides a resource to manage the default AWS Security Group.
  # For EC2 Classic accounts, each region comes with a Default Security Group. Additionally, each VPC created in AWS
  # comes with a Default Security Group that can be managed, but not destroyed. This is an advanced resource, and has
  # special caveats to be aware of when using it.

  # The `aws_default_security_group` behaves differently from normal resources, in that Terraform does not create this
  # resource, but instead "adopts" it into management. We can do this because these default security groups cannot be
  # destroyed, and are created with a known set of default ingress/egress rules.

  # When Terraform first adopts the Default Security Group, it immediately removes all ingress and egress rules in
  # the Security Group. It then proceeds to create any rules specified in the configuration. This step is required so
  # that only the rules specified in the configuration are created.

  # This resource treats its inline rules as absolute; only the rules defined inline are created, and any
  # additions/removals external to this resource will result in diff shown. For these reasons, this resource is
  # incompatible with the `aws_security_group_rule` resource.

  # For more information about Default Security Groups, see the AWS Documentation on Default Security Groups:
  #   * http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html#default-security-group
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) The VPC ID.
  # Note(!): that changing the `vpc_id` will not restore any default security group rules that were modified, added or
  # removed. It will be left in its current state!
  vpc_id = join("", aws_vpc.default.*.id)

  //  # (Optional) Can be specified multiple times for each ingress rule. This argument is processed in
  //  # `attribute-as-blocks` mode.
  //  ## By default allow everything from the instances in the same SG ##
  //  ingress {
  //    # (Required) The start port (or ICMP type number if protocol is "icmp")
  //    from_port = 0
  //    # (Required) The end range port (or ICMP code if protocol is "icmp").
  //    to_port   = 0
  //    # (Required) The protocol. If you select a protocol of "-1" (semantically equivalent to "all", which is not a valid
  //    # value here), you must specify a "from_port" and "to_port" equal to 0. If not icmp, tcp, udp, or "-1" use
  //    # the protocol number:
  //    #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  //    protocol  = -1
  //    # (Optional) If true, the security group itself will be added as a source to this ingress rule.
  //    self      = true
  //  }
  //
  //  # (Optional, VPC only) Can be specified multiple times for each egress rule. This argument is processed in
  //  # `attribute-as-blocks` mode.
  //  ## By default allow all outgoing connections to everywhere ##
  //  egress {
  //    # (Required) The start port (or ICMP type number if protocol is "icmp")
  //    from_port   = 0
  //    # (Required) The end range port (or ICMP code if protocol is "icmp").
  //    to_port     = 0
  //    # (Required) The protocol. If you select a protocol of "-1" (semantically equivalent to "all", which is not a valid
  //    # value here), you must specify a "from_port" and "to_port" equal to 0. If not icmp, tcp, udp, or "-1" use
  //    # the protocol number:
  //    #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  //    protocol    = "-1"
  //    # (Optional) List of CIDR blocks.
  //    cidr_blocks = ["0.0.0.0/0"]
  //  }

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-sg-default", local.vpc_name),
    },
  )

  ### Removing `aws_default_security_group` from your configuration ###
  # Each AWS VPC (or region, if using EC2 Classic) comes with a Default Security Group that cannot be deleted.
  # The `aws_default_security_group` allows you to manage this Security Group, but Terraform cannot destroy it. Removing
  # this resource from your configuration will remove it from your statefile and management, but will not destroy
  # the Security Group. All ingress or egress rules will be left as they are at the time of removal. You can resume
  # managing them via the AWS Console.
}


