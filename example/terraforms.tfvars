# ws@2023 terafform.tfvars

##################################################################
# TFVARS 
##################################################################

# Region
region                  = "<region>"

# Application
name                    = "<name>"
cut_name                = "<name>"

# Environment [info]
environment             = "<env>"
ownerEmail              = "name@mail.com"
team                    = "DevOps"
deployedby              = "Terraform"

# Domain
domain_name_default    = "domain.com"
#domain_name_first      = "dev.domain.com"
#domain_name_second     = "demo.domain.com"


#
public_key = "<ssh-rsa AAA ..>"
