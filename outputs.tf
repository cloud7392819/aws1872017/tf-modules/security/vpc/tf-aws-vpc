#ws@2023 outputs.tf

##################################################################
# VPC
##################################################################

output "vpc_id" {
  description = "The ID of the VPC."
  value       = join("", aws_vpc.default.*.id)
}

output "cidr_block" {
  description = "The CIDR block of the VPC."
  value       = join("", aws_vpc.default.*.cidr_block)
}

output "main_route_table_id" {
  description = "The ID of the main route table associated with this VPC."
  value       = join("", aws_vpc.default.*.main_route_table_id)
}

output "default_network_acl_id" {
  description = "The ID of the network ACL created by default on VPC creation."
  value       = join("", aws_vpc.default.*.default_network_acl_id)
}

output "default_security_group_id" {
  description = "The ID of the security group created by default on VPC creation."
  value       = join("", aws_vpc.default.*.default_security_group_id)
}

output "default_route_table_id" {
  description = "The ID of the route table created by default on VPC creation."
  value       = join("", aws_vpc.default.*.default_route_table_id)
}

output "igw_id" {
  description = "The ID of the Internet Gateway."
  value       = join(",", aws_internet_gateway.default.*.id)
}

##################################################################
## SUBNETS PUBLIC
##################################################################

output "subnets_public" {
  description = "The list of the Public Subnet."
  value       = aws_subnet.public
}

output "subnets_public_ids" {
  description = "The list of the Public Subnet IDs."
  value       = aws_subnet.public.*.id
}

output "subnets_public_cidrs" {
  description = "CIDR blocks of the created public subnets."
  value       = aws_subnet.public.*.cidr_block
}

output "subnets_public_route_tables_ids" {
  description = "IDs of the created public route tables."
  value       = aws_route_table.public.*.id
}

output "subnets_public_id_az_map" {
  description = "Map of the Availability Zones to the Public subnet IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.public.*.id, aws_subnet.public.*.tags.AvailabilityZone)
}

output "subnets_public_id_name_map" {
  description = "Map of the Public Subnet Names to their IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.public.*.id, aws_subnet.public.*.tags.Name)
}

##################################################################
## SUBNETS PRIVATE
##################################################################

output "subnets_private" {
  description = "The list of the Private Subnet."
  value       = aws_subnet.private
}

output "subnets_private_ids" {
  description = "The list of the Private Subnet IDs."
  value       = aws_subnet.private.*.id
}

output "subnets_private_cidrs" {
  description = "CIDR blocks of the created private subnets."
  value       = aws_subnet.private.*.cidr_block
}

output "subnets_private_route_tables_ids" {
  description = "IDs of the created private route tables."
  value       = aws_route_table.private.*.id
}

output "subnets_private_id_az_map" {
  description = "Map of the Availability Zones to the Private subnet IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.private.*.id, aws_subnet.private.*.tags.AvailabilityZone)
}

output "subnets_private_id_name_map" {
  description = "Map of the Private Subnet Names to their IDs."
  # Creates a map from a list of keys and a list of values. The keys must all be of type string, and the length
  # of the lists must be the same.
  value = zipmap(aws_subnet.private.*.id, aws_subnet.private.*.tags.Name)
}

