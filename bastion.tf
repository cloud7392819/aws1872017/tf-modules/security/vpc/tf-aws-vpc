# ws@2023 bastion.tf

##################################################################
# Bastion host SG
##################################################################

##################################################################
## Network/Security resources
##################################################################

resource "aws_security_group" "bastion_host" {

  # Provides a security group resource.
  count = (local.enabled && var.bastion_host_create && local.subnets_public_enabled ) ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-sg-bastion-host-", var.subnets_vpc_name)

  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  description = "Security Group for EC2 Bastion host instances."

  # (Optional, Forces new resource) The VPC ID.
  vpc_id = join ("", aws_vpc.default.*.id)

  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform".
  tags = merge(
    local.tags,
    {
      Name = format("%s-sg-bastion-host", var.subnets_vpc_name),
    },
  )
}

resource "aws_security_group_rule" "bastion_host_outbound" {

  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && var.bastion_host_create && local.subnets_public_enabled) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"

  # (Optional) Description of the rule.
  description = "Allow ALL Egress traffic for EC2 Bastion host instances."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0

  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0

  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"

  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.bastion_host.*.id)
}

resource "aws_security_group_rule" "bastion_host_inbound" {

  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && var.bastion_host_create && local.subnets_public_enabled) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"

  # (Optional) Description of the rule.
  description = "Allow SSH Ingress traffic for EC2 Bastion host instances."

  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = "22"

  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = "22"

  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "tcp"

  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]

  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.bastion_host.*.id)
}

##################################################################
## Get most recent AMI for an ECS-optimized Amazon Linux 2 instance
##################################################################
data "aws_ami" "bastion_host" {

  # Use this data source to get the ID of a registered AMI for use in other resources.
  count = (local.enabled && var.bastion_host_create && local.subnets_public_enabled ) ? 1 : 0

  # (Required) List of AMI owners to limit search. At least 1 value must be specified. Valid values:
  #   * an AWS account ID
  #   * self (the current account)
  #   * an AWS owner alias (e.g. amazon, aws-marketplace, microsoft).
  owners = ["amazon"]

  # (Optional) If more than one result is returned, use the most recent AMI.
  most_recent = true

  # (Optional) One or more name/value pairs to filter off of. There are several valid keys, for a full reference,
  # check out describe-images in the AWS CLI reference:
  #   * http://docs.aws.amazon.com/cli/latest/reference/ec2/describe-images.html

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-*-hvm-*-x86_64-*"]
  }
}

##################################################################
## Create a public and private key pair for login to the EC2 Instances
##################################################################

resource "aws_key_pair" "bastion_host" {

  # Provides an EC2 instance resource. This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning.
  count = (local.enabled && var.bastion_host_create && local.subnets_public_enabled ) ? 1 : 0

  key_name   = "${local.vpc_name}-key-pair-bastion-host"

  public_key = var.public_bastion_host_key

  tags = {
    Scenario = var.scenario
  }
}

##################################################################
## Bastion EC2 Instance
##################################################################

resource "aws_instance" "bastion_host" {

  # Provides an EC2 instance resource. This allows instances to be created, updated, and deleted. 
  # Instances also support provisioning.
  count = (local.enabled && var.bastion_host_create && local.subnets_public_enabled ) ? 1 : 0

  ami = join("", data.aws_ami.bastion_host.*.id)

  instance_type = var.bastion_instance_type
  
  # (Optional) The VPC Subnet ID to launch in.
  subnet_id = matchkeys(
    aws_subnet.public.*.id,
    aws_subnet.public.*.tags.AvailabilityZone,
    [data.aws_availability_zones.default.names[count.index]]
  )[0]
  
  # (Optional) Associate a public ip address with an instance in a VPC. Boolean value.
  associate_public_ip_address = true
  
  #
  key_name = join ("", aws_key_pair.bastion_host.*.id)
  
  # NOTE: If an instance is being created in a VPC, `vpc_security_group_ids` must be used instead of `security_groups`.
  # (Optional, VPC only) A list of security group IDs to associate with.
  #vpc_security_group_ids = [aws_security_group.bastion_host.id]
  vpc_security_group_ids  = [aws_security_group.bastion_host[count.index].id]

  tags = {
    Name     = "${local.vpc_name}-ec2-bastion-host"
    Scenario = var.scenario
  }
}
