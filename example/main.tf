# ws@2023 main.tf

module "vpc_<vpc_name>" {

  source = "git::https://gitlab.com/cloud7392819/aws1872017/tf-modules/security/vpc/tf-aws-vpc.git?ref=tags/0.0.1"
  #source = "./modules/vpc_<vpc_name>"
  
  ##################################################################
  # Common/General settings
  ##################################################################

  # Whether to create the resources (`false` prevents the module from creating any resources).
  # `true && false` prevents the module from creating any resources. 
  module_enabled = false

  name = var.cut_name

  # actually only `vpc_name` would be enough. 
  # See all possible variables and their explanations in `variables.tf`
  # (Required) Name associated with the VPC to distinct from others.
  vpc_name  = "${var.cut_name}-<vpc_name>"
  
  # A mapping of tags to assign to the all underlying resources.
  vpc_tags = {
    #"ENV" = "DEV"
  }

  # The CIDR block for the VPC.
  vpc_cidr_block = "10.1.0.0/16"

  # A tenancy option for instances launched into the VPC.
  vpc_instance_tenancy = "default"

  #Whether to create the Internet Gateway in a VPC to allow public access for resources reside in.
  vpc_enable_internet_gateway = true

  # A boolean flag to enable/disable DNS support in the VPC.
  vpc_enable_dns_support = true

  # A boolean flag to enable/disable DNS hostnames in the VPC.
  vpc_enable_dns_hostnames = true

  ##################################################################
  ## SUBNETS
  ##################################################################

  # Whether to allow sub-module to create the resources.
  # `true && false` prevents the module from creating any resources. 
  #subnets_module_enabled = false

  # (Required) Name associated with the VPC to distinct from others.
  #subnets_vpc_name = var.cut_name

  # Emulation of `depends_on` behavior for the module.
  # Non zero length string can be used to have current module wait for the specified resource.
  subnets_module_depends_on = ""

  # Additional tags to assign to the subnets and other resources.
  subnets_tags = {
    #"ENV" = "DEV"
  }

  # Amount of availability zones to create public/private/.. subnets within the VPC.
  subnets_az_amount = 2

  # To calculate CIDR block for every Subnet - override the maximum amount of subnets will possibly be deployed in every AZ.
  subnets_per_az_max_count = 0

  ##################################################################
  ## SUBNETS PUBLIC
  ##################################################################

  # Whether to allow sub-module to create the resources.
  # `true && false` prevents the module from creating any resources. 
  subnets_public_enabled = false
  
  # How many subnets public in every AZ will be created.
  subnets_public_per_az = 1

  # Additional tags to assign to the subnets and other resources.
  subnets_public_tags = {
    #"ENV" = "DEV"
  }
 
  # The CIDR block of the VPC.
  subnets_public_cidr_block = [
    "10.1.1.0/24", 
    "10.1.2.0/24", 
    "10.1.3.0/24", 
    "10.1.4.0/24",
    "10.1.5.0/24"
  ]

  # Whether to create the Internet Gateway in a VPC to allow public access for resources reside in.
  # `true && false` prevents the module from creating any resources. 
  subnets_public_igw_enabled = false

  # Whether to create the network acl a VPC to allow public access for resources reside in.
  # `true && false` prevents the module from creating any resources. 
  subnets_public_network_acl_enabled = true

  ##################################################################
  ## SUBNETS PRIVATE
  ##################################################################

  # Whether to allow sub-module to create the resources.
  # `true && false` prevents the module from creating any resources. 
  subnets_private_enabled = false
  
  # How many subnets public in every AZ will be created.
  subnets_private_per_az = 1

  # Additional tags to assign to the subnets and other resources.
  subnets_private_tags = {
    #"ENV" = "DEV"
  }
 
  # The CIDR block of the VPC.
  subnets_private_cidr_block = [
    "10.1.11.0/24",
    "10.1.12.0/24", 
    "10.1.13.0/24", 
    "10.1.14.0/24",
    "10.1.15.0/24"
  ]

  # Whether to create the network acl a VPC to allow private access for resources reside in.
  # `true && false` prevents the module from creating any resources. 
  subnets_private_network_acl_enabled = true

  ##################################################################
  ## SUBNETS PRIVATE NAT GATEWAY
  ##################################################################

  # Boolean switch to enable/disable NAT resources.
  # `true && false ` prevents the module from creating any resources. 
  subnets_nat_instance_create = false

  subnets_nat_resource_amount = 1

  # Whether to allocate EIP to the NAT instance.
  # `true && false` prevents the module from creating any resources. 
  subnets_nat_instance_static_eip = true
  
  # What kind of NAT resource to create to allow instances in private subnets to access the Internet.
  # The only possible values are (case insensitive):
  #   * instance (default)
  #   * gateway
  subnets_nat_resource_type = "gateway"

  # Enable/disable NAT resource creation in every AZ for HA scenario.
  # `true && false` prevents the module from creating any resources. 
  subnets_nat_resource_per_az_enabled = false
  
  # The type of NAT EC2 Instance.
  subnets_nat_instance_type = "t3a.micro" 

  ##################################################################
  ## BASTION HOST
  ##################################################################

  # Boolean switch to enable/disable  Bastion host instance.
  # `true && false ` prevents the module from creating any resources. 
  bastion_host_create     = false

  bastion_instance_type   = "t3a.nano" 

  public_bastion_host_key = var.public_key
}