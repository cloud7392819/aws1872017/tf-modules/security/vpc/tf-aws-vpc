#ws@2023 data.tf

##################################################################
# DATA
##################################################################

# Declare the data source to use amount of available AZs per the region in `length()` calculations
data "aws_availability_zones" "default" {

  # The Availability Zones data source allows access to the list of AWS Availability Zones which can be accessed
  # by an AWS account within the region configured in the provider.
  # This is different from the `aws_availability_zone` (singular) data source, which provides some details about
  # a specific availability zone.
  #count = (local.enabled && local.subnets_enabled) ? 1 : 0

  # (Optional) Allows to filter list of Availability Zones based on their current state. Can be either:
  #   * available
  #   * information
  #   * impaired
  #   * unavailable.
  # By default the list includes a complete set of Availability Zones to which the underlying AWS account has access,
  # regardless of their state.
  state = "available"
}



