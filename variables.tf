#ws@2023 variables.tf

##################################################################
# Common/General settings
##################################################################

variable "module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

# Application cut name
variable "name" {
  description = "The application cut name"
  default     = null
}

variable "namespace" {
  description = "Namespace for resource names"
  default     = null
  type        = string
}

variable "scenario" {
  description = "Scenario name for tags"
  default     = null
  type        = string
}

variable "environment" {
  description = "Environment for deployment (like dev or staging ..)"
  default     = null
  type        = string
}

variable "module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default     = ""
}

variable "module_tags" {
  description = "Additional mapping of tags to assign to the all linked resources."
  type        = map(string)
  default     = {}
}

##################################################################
# VPC
##################################################################

variable "vpc_name" {
  description = "(Required) Name associated with the VPC to distinct from others."
  type        = string
  default     = null
}

variable "vpc_tags" {
  description = "A mapping of tags to assign to the all underlying resources."
  type        = map(string)
  default     = {}
}

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC."
  type        = string
  default     = "default"
}

variable "vpc_enable_internet_gateway" {
  description = "Whether to create the Internet Gateway in a VPC to allow public access for resources reside in."
  type        = bool
  default     = true
}

variable "vpc_enable_dns_support" {
  description = "A boolean flag to enable/disable DNS support in the VPC."
  type        = bool
  default     = true
}

variable "vpc_enable_dns_hostnames" {
  description = "A boolean flag to enable/disable DNS hostnames in the VPC."
  type        = bool
  default     = true
}

##################################################################
# SUBNETS
##################################################################

variable "subnets_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

variable "subnets_public_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

variable "subnets_private_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

variable "subnets_database_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

variable "subnets_module_depends_on" {
  description = "Emulation of `depends_on` behavior for the module."
  type        = any
  # Non zero length string can be used to have current module wait for the specified resource.
  default = null
}

variable "subnets_tags" {
  description = "Additional tags to assign to the subnets and other resources."
  type        = map(string)
  default     = {}
}

variable "subnets_public_tags" {
  description = "Additional tags to assign to the subnets and other resources."
  type        = map(string)
  default     = {}
}

variable "subnets_private_tags" {
  description = "Additional tags to assign to the subnets and other resources."
  type        = map(string)
  default     = {}
}

variable "subnets_database_tags" {
  description = "Additional tags to assign to the subnets and other resources."
  type        = map(string)
  default     = {}
}

variable "subnets_vpc_id" {
  description = "(Required) VPC ID where subnets will be created."
  type        = string
  default     = ""
}

variable "subnets_vpc_name" {
  description = "(Required) Name associated with the VPC to distinct from others."
  type        = string
  default     = ""
}

variable "subnets_vpc_cidr_block" {
  description = "The CIDR block of the VPC."
  # if not specified - will try to get from VPC parameters.
  type    = string
  default = ""
}

# Cidr block subnets public
variable "subnets_public_cidr_block" {
  description = "Available cidr blocks for subnets public."
  type        = list(string)
  default     = [
    "10.1.1.0/24",
    "10.1.2.0/24",
    "10.1.3.0/24",
    "10.1.4.0/24",
    "10.1.5.0/24"
  ]
}

# Cidr block subnets private
variable "subnets_private_cidr_block" {
  description = "Available cidr blocks for subnets private."
  type        = list(string)
  default     = [
    "10.1.11.0/24",
    "10.1.12.0/24",
    "10.1.13.0/24",
    "10.1.14.0/24",
    "10.1.15.0/24"
  ]
}

variable "subnets_igw_enabled" {
  description = "Whether to use the specified Internet Gateway ID to configure routes for Public subnets."
  type        = bool
  default     = false
}

variable "subnets_public_igw_enabled" {
  description = "Whether to use the specified Internet Gateway ID to configure routes for Public subnets."
  type        = bool
  default     = false
}

variable "subnets_public_network_acl_enabled" {
  description = "Whether to use the specified network acl to configure routes for Public subnets."
  type        = bool
  default     = false
}

variable "subnets_private_network_acl_enabled" {
  description = "Whether to use the specified network acl to configure routes for Private subnets."
  type        = bool
  default     = false
}

variable "subnets_database_network_acl_enabled" {
  description = "Whether to use the specified network acl to configure routes for Database subnets."
  type        = bool
  default     = false
}

variable "subnets_igw_id" {
  description = "Internet Gateway ID the public route table will point to."
  type        = string
  default     = ""
}

variable "subnets_az_amount" {
  description = "Amount of availability zones to create public/private subnets within the VPC."
  type        = number
  default     = 0
}

variable "subnets_per_az_max_count" {
  description = "To calculate CIDR block for every Subnet - override the maximum amount of subnets will possibly be deployed in every AZ."
  type        = number
  default     = 0
}

variable "subnets_public_per_az" {
  description = "How many public subnets in every AZ will be created."
  type        = number
  default     = 0
}

variable "subnets_private_per_az" {
  description = "How many private subnets in every AZ will be created."
  type        = number
  default     = 0
}

variable "subnets_database_per_az" {
  description = "How many private subnets in every AZ will be created."
  type        = number
  default     = 0
}

##################################################################
## NAT GATEWAY
##################################################################

variable "subnets_nat_instance_create" {
  description = "Boolean switch to enable/disable NAT instance."
  type        = bool
  default     = false
}

variable "subnets_nat_instance_static_eip" {
  description = "Whether to allocate EIP to the NAT instance."
  type        = bool
  default     = false
}

variable "subnets_nat_resource_amount" {
  description = "Amount of nat resource to create public/private subnets within the VPC."
  type        = number
  default     = 0
}

variable "subnets_nat_resource_per_az_enabled" {
  description = "Enable/disable NAT resource creation in every AZ for HA scenario."
  type        = bool
  default     = false
}

variable "subnets_nat_resource_type" {
  description = "What kind of NAT resource to create to allow instances in private subnets to access the Internet."
  # The only possible values are (case insensitive):
  #   * instance (default)
  #   * gateway
  type    = string
  default = "instance"
}

variable "subnets_nat_instance_type" {
  description = "The type of NAT EC2 Instance."
  type        = string
  default     = "t3a.micro"
}

##################################################################
## BASTION HOST
##################################################################

variable "bastion_host_create" {
  description = "Boolean switch to enable/disable Bastion host instance."
  type        = bool
  default     = false
}

variable "public_bastion_host_key" {
  description = "Public key for SSH access to EC2 Bastion host instances"
  type        = string
  default     = null
}

variable "bastion_instance_type" {
  description = "EC2 Bastion host instances type"
  type        = string
  default     = "t3a.nano"
}

