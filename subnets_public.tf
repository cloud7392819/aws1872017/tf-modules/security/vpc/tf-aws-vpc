#ws@2023 subnets_public.tf

##################################################################
# Manage VPC's "Public" Subnets
##################################################################

## Create public subnets
resource "aws_subnet" "public" {

  # Provides an VPC subnet resource.
  count = (local.enabled && local.subnets_public_enabled) ? local.subnets_public_amount : 0

  # (Required) The VPC ID.
  vpc_id = join ("", aws_vpc.default.*.id)

  # (Optional) The AZ for the subnet.
  # availability_zone = element(data.aws_availability_zones.default.names, count.index)
  availability_zone = data.aws_availability_zones.default.names[floor(count.index / var.subnets_public_per_az)]
  
  # (Required) The CIDR block for the subnet.
  cidr_block = element(var.subnets_public_cidr_block, count.index)

  # (Optional) Specify true to indicate that instances launched into the subnet should be assigned a public IP address.
  map_public_ip_on_launch = true

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags` and assign some more specific for the current resource.
  tags = merge(
    local.tags,
    {
      SubnetType       = "public"

      AvailabilityZone = data.aws_availability_zones.default.names[floor(count.index / var.subnets_public_per_az)]
      
      # Name = "${var.name}-${var.vpc_name}-subnet-public-${element(data.aws_availability_zones.default.names, count.index)}-${var.environment}"
      Name = format(
        "%s-subnet-public-%s",
        local.vpc_name,
        data.aws_availability_zones.default.names[floor(count.index / var.subnets_public_per_az)]
      )

    },
  )

  lifecycle {
    # Ignore tags added by kops or kubernetes
    ignore_changes = [
      tags.kubernetes,
      tags.SubnetType,
    ]
  }
}

##################################################################
## --- ROUTE TABLE (RT) AND ROUTE ---
##################################################################

##################################################################
### Create route table
##################################################################

resource "aws_route_table" "public" {

  # Provides a resource to create a VPC routing table.

  # NOTE(1) on Route Tables and Routes:
  # Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined in-line.
  # At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources. Doing so
  # will cause a conflict of rule settings and will overwrite rules.

  # NOTE(2) on `gateway_id` and `nat_gateway_id`:
  # The AWS API is very forgiving with these two attributes and the `aws_route_table` resource can be created
  # with a NAT ID specified as a Gateway ID attribute. This will lead to a permanent diff between your configuration
  # and statefile, as the API returns the correct parameters in the returned route table. If you're experiencing
  # constant diffs in your `aws_route_table` resources, the first thing to check is whether or not you're specifying
  # a NAT ID instead of a Gateway ID, or vice-versa.

  # NOTE(3) on `propagating_vgws` and the `aws_vpn_gateway_route_propagation` resource:
  # If the `propagating_vgws` argument is present, it's not supported to also define route propagations using
  # `aws_vpn_gateway_route_propagation`, since this resource will delete any propagating gateways not explicitly
  # listed in `propagating_vgws`. Omit this argument when defining route propagation using the separate resource.
  count = (local.enabled && local.subnets_public_enabled && local.subnets_public_amount > 0) ? 1 : 0

  # (Required) The VPC ID.
  vpc_id = join ("", aws_vpc.default.*.id)

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format(
        "%s-subnets-public-rt", 
        local.vpc_name
      ),
    },
  )
}

##################################################################
### Create route for access to inet
##################################################################

resource "aws_route" "public" {

  # Provides a resource to create a routing table entry (a route) in a VPC routing table.

  # NOTE on Route Tables and Routes:
  # Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined in-line.
  # At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources. Doing so
  # will cause a conflict of rule settings and will overwrite rules.
  count = (local.subnets_public_enabled && local.subnets_public_amount > 0) ? 1 : 0
  #count = (local.enabled && local.subnets_public_enabled && local.subnets_public_amount > 0 && var.subnets_public_igw_enabled) ? 1 : 0

  # (Required) The ID of the routing table.
  route_table_id = join("", aws_route_table.public.*.id)

  # (Optional) The destination CIDR block.
  destination_cidr_block = "0.0.0.0/0"
  
  # (Optional) Identifier of a VPC internet gateway or a virtual private gateway.
  gateway_id = join("", aws_internet_gateway.default.*.id)
}

# Create route table association
resource "aws_route_table_association" "public" {

  # Provides a resource to create an association between a subnet and routing table.
  count = (local.enabled && local.subnets_public_enabled) ? local.subnets_public_amount : 0

  # (Required) The subnet ID to create an association.
  subnet_id = aws_subnet.public[count.index].id

  # (Required) The ID of the routing table to associate with.
  route_table_id = join("", aws_route_table.public.*.id)

  depends_on    = [
    aws_route_table.public,
  ]

}

##################################################################
### Create network acl
##################################################################

resource "aws_network_acl" "public" {

  # Provides an network ACL resource. You might set up network ACLs with rules similar to your security groups in order
  # to add an additional layer of security to your VPC.

  # NOTE on Network ACLs and Network ACL Rules:
  # Terraform currently provides both a standalone Network ACL Rule resource and a Network ACL resource with rules
  # defined in-line. At this time you cannot use a Network ACL with in-line rules in conjunction with
  # any Network ACL Rule resources. Doing so will cause a conflict of rule settings and will overwrite rules.
  count = (local.enabled && local.subnets_public_enabled && local.subnets_public_amount > 0 && var.subnets_public_network_acl_enabled) ? 1 : 0

  # (Required) The ID of the associated VPC.
  vpc_id = join ("", aws_vpc.default.*.id)

  # (Optional) A list of Subnet IDs to apply the ACL to
  subnet_ids = aws_subnet.public.*.id

  # (Optional) Specifies an egress rule. Parameters defined below. This argument is processed in
  # attribute-as-blocks (https://www.terraform.io/docs/configuration/attr-as-blocks.html) mode.
  egress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  # (Optional) Specifies an ingress rule. Parameters defined below. This argument is processed in
  # attribute-as-blocks (https://www.terraform.io/docs/configuration/attr-as-blocks.html) mode.
  ingress {
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
    protocol   = "-1"
  }

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      Name = format(
        "%s-subnets-public-acl", 
        local.vpc_name
      ),
    },
  )
}

