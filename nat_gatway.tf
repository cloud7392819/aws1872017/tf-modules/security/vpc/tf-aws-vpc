# ws@2023 nat_gateway.tf

##################################################################
# NAT Gateway 
##################################################################

##################################################################
## Creates one Elastic IP per AZ (one for each NAT Gateway in each AZ)
##################################################################

resource "aws_eip" "nat_gateway" {

  # Provides an Elastic IP resource.

  # Note(1): EIP may require IGW to exist prior to association. Use depends_on to set an explicit dependency on the IGW.

  # Note(2): Do not use `network_interface` to associate the EIP to `aws_lb` or `aws_nat_gateway` resources. Instead use
  # the `allocation_id` available in those resources to allow AWS to manage the association, otherwise you will see
  # `AuthFailure` errors.
  count = (local.enabled && local.subnets_nat_resource_create) ? local.nat_gateways_amount : 0

  # See:
  #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
  depends_on = [
    local.subnets_module_depends_on
  ]

  # (Optional) Boolean if the EIP is in a VPC or not.
  #vpc = true

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.default.names[count.index],
      Name             = format("%s-%s-nat-gateway-eip", local.vpc_name, data.aws_availability_zones.default.names[count.index]),
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}

##################################################################
## Creates one NAT Gateway per AZ
##################################################################

resource "aws_nat_gateway" "nat_gateway" {

  # Provides a resource to create a VPC NAT Gateway.
  count = (local.enabled && local.subnets_nat_resource_create) ? local.nat_gateways_amount : 0

  # Note: It's recommended to denote that the NAT Gateway depends on the Internet Gateway for the VPC in which
  # the NAT Gateway's subnet is located.
  # See:
  #   * https://discuss.hashicorp.com/t/tips-howto-implement-module-depends-on-emulation/2305
  depends_on = [
    var.subnets_module_depends_on
  ]

  # (Required) The Allocation ID of the Elastic IP address for the gateway.
  allocation_id = aws_eip.nat_gateway[count.index].id

  # (Required) The Subnet ID of the subnet in which to place the gateway.
  subnet_id = matchkeys(
    aws_subnet.public.*.id,
    aws_subnet.public.*.tags.AvailabilityZone,
    [data.aws_availability_zones.default.names[count.index]]
  )[0]

  # (Optional) A mapping of tags to assign to the resource.
  # Override the "Name" tag from `local.vpc_tags`.
  tags = merge(
    local.tags,
    {
      AvailabilityZone = data.aws_availability_zones.default.names[count.index],
      Name             = format("%s-%s-nat-gateway", local.vpc_name, data.aws_availability_zones.default.names[count.index]),
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}

# if NAT gateway enabled, every route from Private subnets must points to either NAT gateway in the same AZ (in case
# of High Availability config) or the single NAT gateway created in one of available AZs.

resource "aws_route" "private_to_nat_gateway" {

  # Provides a resource to create a routing table entry (a route) in a VPC routing table.

  # NOTE on Route Tables and Routes:
  # Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined in-line.
  # At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources. Doing so
  # will cause a conflict of rule settings and will overwrite rules.
  #count = (local.subnets_nat_resource_create&& local.nat_gateways_amount > 0) ? var.subnets_az_amount : 0
  count = (local.enabled && local.subnets_nat_resource_create && local.nat_gateways_amount > 0) ? var.subnets_nat_resource_amount : 0

  # (Required) The ID of the routing table.
  route_table_id = aws_route_table.private[0].id

  # (Optional) The destination CIDR block.
  destination_cidr_block = "0.0.0.0/0"

  # (Optional) Identifier of a VPC NAT gateway.
  # Select NAT Gateway either those located in the same AZ (in case of HA config), or the single one.
  nat_gateway_id = local.nat_gateways_amount > 1 ? matchkeys(aws_nat_gateway.nat_gateway.*.id, aws_nat_gateway.nat_gateway.*.tags.AvailabilityZone, [aws_route_table.private[count.index].tags.AvailabilityZone])[0] : aws_nat_gateway.nat_gateway[0].id

  depends_on             = [
    aws_route_table.private
  ]
}

##################################################################
## Route to the internet using the NAT Gateway
##################################################################

# resource "aws_route_table" "private" {

#   count = var.subnets_nat_resource_amount
 
#   vpc_id = join ("", aws_vpc.this.*.id)

#   route {
#     cidr_block     = "0.0.0.0/0"
#     nat_gateway_id = aws_nat_gateway.nat_gateway[0].id
#     #nat_gateway_id = aws_nat_gateway.nat_gateway[count.index].id
#   }

#   tags = {
#     Name     = "${var.namespace}-PrivateRouteTable-${count.index}_${var.environment}"
#     Scenario = var.scenario
#   }
# }

##################################################################
## Associate Route Table with Private Subnets
##################################################################

# resource "aws_route_table_association" "private" {

#   count          = var.subnets_nat_resource_amount

#   subnet_id      = aws_subnet.private[count.index].id
#   route_table_id = aws_route_table.private[count.index].id
# }

