#ws@2023 rt.tf

##################################################################
# Setup VPC's "Main" (or "Default") Route Table
##################################################################

resource "aws_default_route_table" "default" {

  # Provides a resource to manage a Default VPC Routing Table.
  # Each VPC created in AWS comes with a Default Route Table that can be managed, but not destroyed. This is an advanced
  # resource, and has special caveats to be aware of when using it. It is recommended you do not use both
  # `aws_default_route_table` to manage the default route table and use the `aws_main_route_table_association`, due to
  # possible conflict in routes.

  # The `aws_default_route_table` behaves differently from normal resources, in that Terraform does not create this
  # resource, but instead attempts to "adopt" it into management. We can do this because each VPC created has
  # a Default Route Table that cannot be destroyed, and is created with a single route.

  # When Terraform first adopts the Default Route Table, it immediately removes all defined routes. It then proceeds
  # to create any routes specified in the configuration. This step is required so that only the routes specified in
  # the configuration present in the Default Route Table.

  # For more information about Route Tables, see the AWS Documentation on Route Tables:
  #   * http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Route_Tables.html#Route_Replacing_Main_Table

  # For more information about managing normal Route Tables in Terraform, see our documentation on `aws_route_table`:
  #   * https://www.terraform.io/docs/providers/aws/r/route_table.html

  # NOTE(!) on Route Tables and Routes:
  #   Terraform currently provides both a standalone Route resource and a Route Table resource with routes defined
  #   in-line. At this time you cannot use a Route Table with in-line routes in conjunction with any Route resources.
  #   Doing so will cause a conflict of rule settings and will overwrite routes.
  count = local.enabled ? 1 : 0

  # (Required) The ID of the Default Routing Table.
  default_route_table_id = join("", aws_vpc.default.*.default_route_table_id)

  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-rt-main", local.vpc_name),
    },
  )
}




