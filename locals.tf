#ws@2023 locals.tf

##################################################################
# VPC
##################################################################

locals {

  # vpc
  vpc_enabled = lower(var.module_enabled) ? true : false

  # create
  enabled = var.module_enabled ? true : false
  create  = var.module_enabled ? true : false

  vpc_name = var.vpc_name
  #vpc_name = replace(var.vpc_name, "-vpc", "") == var.vpc_name ? format("%s-vpc", var.vpc_name) : var.vpc_name

  # subnets
  subnets_enabled = lower(var.subnets_module_enabled) ? true : false

  subnets_public_enabled = lower(var.subnets_public_enabled) ? true : false
  subnets_private_enabled = lower(var.subnets_private_enabled) ? true : false

  vpc_cidr_block = length(var.vpc_cidr_block) > 0 ? var.vpc_cidr_block : join("", aws_vpc.default.*.cidr_block)
  #vpc_cidr_block = length(var.subnets_vpc_cidr_block) > 0 ? var.subnets_vpc_cidr_block : join("", data.aws_vpc.default.*.cidr_block)

  # calculate amount of public/private/.. subnets need to be created
  subnets_public_amount  = (var.subnets_az_amount * var.subnets_public_per_az)
  subnets_private_amount = (var.subnets_az_amount * var.subnets_private_per_az)

  # nat
  subnets_nat_resource_create = lower(var.subnets_nat_instance_create) ? true : false
  subnets_nat_instance_create = lower(var.subnets_nat_instance_create) ? true : false

  # calculate amount of NAT resources
  nat_resources_amount = var.subnets_nat_instance_create ? (var.subnets_nat_resource_per_az_enabled ? var.subnets_az_amount : 1) : 0
  nat_gateways_amount  = lower(var.subnets_nat_resource_type) == "gateway" ? local.nat_resources_amount : 0
  nat_instances_amount = lower(var.subnets_nat_resource_type) == "instance" ? local.nat_resources_amount : 0

  # maximum number of subnets which can be created. This constant is being used for CIDR blocks calculation.
  max_total_subnets = var.subnets_per_az_max_count > 0 ? (var.subnets_az_amount * var.subnets_per_az_max_count) : ((local.subnets_public_amount + local.subnets_private_amount > 16) ? (local.subnets_public_amount + local.subnets_private_amount) : 16)

  subnets_module_depends_on = var.subnets_module_depends_on

  tags = merge(
    var.vpc_tags,
    {
      terraform = true,
      Create = "terraform with resource",
    },
  )
}


