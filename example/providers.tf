# ws@2023 providers.tf

provider "aws" {

  region = var.region

  #skip_region_validation = true

  default_tags {

    tags = {

      "Environment" = var.environment
      "Team"        = var.team
      "DeployedBy"  = var.deployedby
      "Application" = var.name
      "OwnerEmail"  = var.ownerEmail
      "Region"      = var.region
    }
  }
}

provider "aws" {

  alias  = "virginia"
  region = "us-east-1"

  default_tags {

    tags = {

      "Environment" = var.environment
      "Team"        = var.team
      "DeployedBy"  = var.deployedby
      "Application" = var.name
      "OwnerEmail"  = var.ownerEmail
      "Region"      = "us-east-1"
    }
  }
}